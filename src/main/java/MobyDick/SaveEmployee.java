package MobyDick;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.client.Customer;
import infernalWhaler.model.client.Invoice;
import infernalWhaler.model.employee.Contract;
import infernalWhaler.model.employee.Employee;
import infernalWhaler.model.employee.EmployeeWorkDetails;
import infernalWhaler.model.project.Location;
import infernalWhaler.model.project.Project;
import infernalWhaler.model.project.ServiceProducts;
import infernalWhaler.model.project.Task;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Scanner;

public class SaveEmployee {

    // Variables
    private EntityManager em;

    // Constructor
    public SaveEmployee(EntityManager em) {
        this.em = em;
    }

    // Method

    // EMP
    private Contract makeNewContract(Scanner kbd) {
//        System.out.println("Give StartUp Salary: ");
//        double salary = kbd.nextDouble();
//        System.out.println("Give Contract type: Fix or Freelance");
//        Contract contract;
//        kbd.nextLine();
//        String cType = kbd.nextLine();
//        if (cType.equalsIgnoreCase("fix")) {
//            Contract.ContractType fix = Contract.ContractType.CONTRACT;
//            contract = new Contract(salary, fix);
//        } else {
//            Contract.ContractType free = Contract.ContractType.FREELANCE;
//            contract = new Contract(salary, free);
//        }
//        return contract;
       return null;
    }

    private EmployeeWorkDetails makeNewEMPdetails(Scanner kbd) {
        return new EmployeeWorkDetails();
    }

    private LocalDate inputBday(Scanner kbd) {
//        System.out.println("Give Birthday: ");
//        System.out.println("YYYY");
//        int y = kbd.nextInt();
//        System.out.println("MM");
//        int m = kbd.nextInt();
//        System.out.println("DD");
//        int d = kbd.nextInt();
//        return LocalDate.of(y, m, d);
        return null;
    }

    public void makeNewEMP(Scanner kbd) {
        System.out.println("Importing a new Employee: \nGive First Name");
        String fName = kbd.nextLine();
        System.out.println("Give Last Name: ");
        String lName = kbd.nextLine();
        System.out.println("Give Phone number: ");
        String phone = kbd.nextLine();
        System.out.println("Give Address: ");
        String address = kbd.nextLine();
        System.out.println("Give City: ");
        String city = kbd.nextLine();
        LocalDate bday = inputBday(kbd);

        Employee employee = new Employee(fName, lName, phone, address, city, bday);

        Contract contract = makeNewContract(kbd);
        employee.addContract(contract);

        EmployeeWorkDetails employeeWorkDetails = makeNewEMPdetails(kbd);
        employeeWorkDetails.addEmployee(employee);


        em.persist(employeeWorkDetails);

    }


    // TASK
    private Task addTasks(Scanner kbd) {
        System.out.println("Give Task name: ");
        String taskName = kbd.nextLine();
        Task task = new Task(taskName);
        return task;
    }


    public void addTAskToEMP(Scanner kbd) {
        System.out.println("Give Task description:");
        Task inputTask = new Task(kbd.nextLine());
        System.out.println("Give ID of EMP: ");
        long id = kbd.nextLong();
        EmployeeWorkDetails emp = em.find(EmployeeWorkDetails.class, id);
        if (emp != null) {
            emp.addTasks(inputTask);
            em.persist(inputTask);
        }
    }

    public void addMaterialsToTask(Scanner kbd){
        System.out.println("Give ID of Task: ");
        long idTask = kbd.nextLong();
        Task task = em.find(Task.class, idTask);

        System.out.println("Give ID of material: ");
        long idMat = kbd.nextLong();
        ServiceProducts mat = em.find(ServiceProducts.class,idMat);
        task.addMaterials(mat);
    }

    public ServiceProducts addMaterial(Scanner kbd){
        System.out.println("Give Material name: ");
        String name = kbd.next();
        System.out.println("Give Price: ");
        double price = kbd.nextDouble();
        System.out.println("Give Working hours: ");
        double hours = kbd.nextDouble();

        ServiceProducts serviceProducts = new ServiceProducts(name, price, hours);
        em.persist(serviceProducts);
        return serviceProducts;
    }

    public void addLocationAndTasksToEMP(Scanner kbd) {
        System.out.println("Give ID of EMP: ");
        long id = kbd.nextLong();
        EmployeeWorkDetails emp = em.find(EmployeeWorkDetails.class, id);
        if (emp != null) {
            System.out.println("Give ID of Task");
            long idTask = kbd.nextLong();
            Task task = em.find(Task.class, idTask);
            emp.addTasks(task);
        }
    }

    // LOCATION
    public void addLocationAndTasks(Scanner kbd) {
        System.out.println("Give Location: ");
        String placeName = kbd.nextLine();
        Location location = new Location(placeName);

        boolean input = true;
        while (input) {
            Task task = addTasks(kbd);
            if (task.getNameTask().equalsIgnoreCase("")) {
                input = false;
            } else {
                location.addTask(task);
            }
        }
        em.persist(location);
    }

    // PROJECT
    public Project addNewProject(Scanner kbd) {
        System.out.println("Give New Project name: ");
        String projectName = kbd.nextLine();
        Project project = new Project(projectName);
        em.persist(project);
        return project;
    }

    public void addProjectToTasks(Scanner kbd) {
        System.out.println("Give Project ID: ");
        long projectID = kbd.nextLong();
        Project project = em.find(Project.class, projectID);

        System.out.println("Give Task ID: ");
        long taskID = kbd.nextLong();
        Task task = em.find(Task.class, taskID);
        project.addTasks(task);

    }       // AddTasksToProject

    // INVOICE
    public Invoice addInvoice(Scanner kbd) {
        System.out.println("Give Amount of invoice: ");
        double price = kbd.nextDouble();
        return new Invoice(price);
    }

    public void addInvoiceToProject(Scanner kbd) {
        System.out.println("Give Project ID: ");
        long projectID = kbd.nextLong();
        Project project = em.find(Project.class, projectID);
        project.addInvoices(addInvoice(kbd));
    }


    // CUSTOMER
    public void addNewCustomer(Scanner kbd){
        System.out.println("Give Name New Customer: ");
        String nameC = kbd.nextLine();
        System.out.println("Give Address: ");
        String address = kbd.nextLine();
        System.out.println("Give City: ");
        String city = kbd.nextLine();
        Customer customer = new Customer(nameC, address, city);
        System.out.println("Link your Customer to project: ");
        long idProject = kbd.nextLong();
        Project project = em.find(Project.class, idProject);
        customer.addProjects(project);
        em.persist(customer);
    }

    public static void main(String[] args) {


    EmployeeWorkDetails emp = new EmployeeWorkDetails();

    double sick = 5 * emp.addOneSickDay();
        System.out.println(sick);



    }
}
