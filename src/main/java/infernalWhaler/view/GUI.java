package infernalWhaler.view;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.employee.Contract;
import infernalWhaler.model.employee.Employee;
import infernalWhaler.model.employee.EmployeeWorkDetails;
import infernalWhaler.model.project.Location;
import infernalWhaler.model.project.Project;
import infernalWhaler.model.project.ServiceProducts;
import infernalWhaler.model.project.Task;
import infernalWhaler.service.employeeServcie.EmployeeServcie;
import infernalWhaler.service.projectService.LocationService;
import infernalWhaler.service.projectService.ProjectService;
import infernalWhaler.service.projectService.ServiceProductsService;
import infernalWhaler.service.projectService.TaskService;

import javax.persistence.EntityManager;
import java.sql.SQLOutput;
import java.time.LocalDate;
import java.util.Scanner;

public class GUI {

    // Variables
    private EmployeeServcie employeeServcie;
    private TaskService taskService;
    private ProjectService projectService;
    private LocationService locationService;
    private ServiceProductsService serviceProductsService;

    private Scanner kbd;
    private EntityManager em = EmProvidor.getEntityManager();


    // Constructor
    public GUI() {
        employeeServcie = new EmployeeServcie();
        taskService = new TaskService();
        projectService = new ProjectService();
        locationService = new LocationService();
        serviceProductsService = new ServiceProductsService();
        kbd = new Scanner(System.in);
    }

    // Methods
    public void start() {

        readCommand();

    }


    // MENU's
    private void showMenuEMP() {
        System.out.println("EMP GUI commands:\n\n" +
                "save:   Import EMP" +
                "\nread:   See chosen EMP" +
                "\nupdate: update chosen EMP" +
                "\ndelete: Delete chosen EMP" +
                "\nadd: Add Task to EMP" +
                "\nquit:   End program\n\n");
    }

    private void showMenuServiceProducts() {
        System.out.println("EMP GUI commands:\n\n" +
                "save:   Import EMP" +
                "\nread:   See chosen SP" +
                "\nupdate: update chosen SP" +
                "\ndelete: Delete chosen SP" +
                "\nadd: Add Task to SP" +
                "\nquit:   End program\n\n");
    }


    private void showMenuLocation() {
        System.out.println("EMP GUI commands:\n\n" +
                "save:   Import Location" +
                "\nread:   See chosen Location" +
                "\nupdate: update chosen Location" +
                "\ndelete: Delete chosen Location" +
                "\nquit:   End program\n\n");
    }

    private void showMenuTask() {
        System.out.println("EMP GUI commands:\n\n" +
                "save:   Import Task" +
                "\nread:   See chosen Task" +
                "\nupdate: update chosen Task" +
                "\ndelete: Delete chosen Task" +
                "\nquit:   End program\n\n");
    }

    private void showMenu() {
        System.out.println("EMP GUI commands:\n\n" +
                "Employee : menu " +
                "\nProject : menu");
    }

    private void showMenuProject() {
        System.out.println("EMP GUI commands:\n\n" +
                "Location : menu " +
                "Project : menu " +
                "\nTask : menu");
    }

    private void readCommand() {
        boolean readCommand = true;
        while (readCommand) {
            System.out.println("Give your command: ");
            String command = kbd.nextLine();
            if (command.equalsIgnoreCase("SP")) {
                serviceProductsConditions(command);
            }


            if (command.equalsIgnoreCase("quit")) {
                System.out.println("GoodBye my Builder Lord");
                em.close();
                em.getEntityManagerFactory().close();
                break;
            }
        }
    }

    // SERVICEPRODUCTS << Functions >>
    private boolean serviceProductsConditions(String command) {
        showMenuServiceProducts();
        command = kbd.nextLine();
        if (command.equalsIgnoreCase("quit")) {
            System.out.println("GoodBye my Builder Lord");
            em.close();
            em.getEntityManagerFactory().close();
            return true;
        } else if (command.equalsIgnoreCase("save")) {
            // save
            saveSP();
        } else if (command.equalsIgnoreCase("add")) {
            // read
            addSPtoTask();
        } else if (command.equalsIgnoreCase("read")) {
            // read
//            readSP();
        } else if (command.equalsIgnoreCase("update")) {
            // update
            updateSP();
        } else if (command.equalsIgnoreCase("delete")) {
            // delete
//            deleteSP();
        }
        return false;
    }

    private void updateSP() {
        System.out.println("Give\n << Price Materials >> \n << Hours Workers >> \n  << Price Hours >> ");
        String whatToChange = kbd.nextLine();
        System.out.println("Give ID");
        long id = kbd.nextLong();
        System.out.println("Amount");
        double amount = kbd.nextDouble();
        serviceProductsService.updateServiceProductsFromDB(id, whatToChange, amount);
    }


    private void addSPtoTask() {
        System.out.println("Give ID of Service Products to link to Task");
        long id = kbd.nextLong();
        kbd.nextLine();
        System.out.println("Give task Name");
        String name = kbd.nextLine();
        serviceProductsService.addTaskToServiceProducts(id, name);
    }

    private void saveSP() {
        System.out.println("Add new Service Product\n Give Material name");
        String matName = kbd.nextLine();
        System.out.println("Give Price of materials");
        double price = kbd.nextDouble();
        System.out.println("Give total hours needed for service ");
        double hours = kbd.nextDouble();
        serviceProductsService.saveServiceProductsToDB(new ServiceProducts(matName, price, hours));

    }


    // PROJECT << Functions >>
    private boolean projectConditions(String command) {
        showMenuLocation();
        command = kbd.nextLine();
        if (command.equalsIgnoreCase("quit")) {
            System.out.println("GoodBye my Builder Lord");
            em.close();
            em.getEntityManagerFactory().close();
            return true;
        } else if (command.equalsIgnoreCase("save")) {
            // save
            saveProject();
        } else if (command.equalsIgnoreCase("read")) {
            // read
            readProject();
        } else if (command.equalsIgnoreCase("update")) {
            // update
            updateProject();
        } else if (command.equalsIgnoreCase("delete")) {
            // delete
            deleteProject();
        }
        return false;
    }

    private void deleteProject() {
        System.out.println("Give name of Project to Delete");
        String projectName = kbd.nextLine();
        projectService.deleteProjectFromDB(projectName);
    }

    private void updateProject() {
        System.out.println("Give project name");
        String projectName = kbd.nextLine();
        System.out.println("Give TaskName: ");
        String taskName = kbd.nextLine();
        projectService.addTaskToProject(projectName, taskName);
    }

    private void readProject() {
        System.out.println("Give Project Name or 'ALL' to get all projects");
        String projectN = kbd.nextLine();
        if (projectN.equalsIgnoreCase("all")) {
            projectService.readProjectFromDB();
        } else if (!projectN.equalsIgnoreCase("all")) {
            projectService.readProjectFromDB(projectN);
        }
    }

    // Same Time add tasks via location
    private void saveProject() {
        System.out.println("Give Project name: ");
        String projectName = kbd.nextLine();
        projectService.saveProjectToDB(new Project(projectName));
    }


    // Task << Functions >>
    private boolean taskConditions(String command) {
        if (command.equalsIgnoreCase("Task")) {
            showMenuTask();
            command = kbd.nextLine();
            if (command.equalsIgnoreCase("quit")) {
                System.out.println("GoodBye my Builder Lord");
                em.close();
                em.getEntityManagerFactory().close();
                return true;
            } else if (command.equalsIgnoreCase("save")) {
                // save
                saveTask();
            } else if (command.equalsIgnoreCase("read")) {
                // read
                readTask();
            } else if (command.equalsIgnoreCase("update")) {
                // update
                updateTask();
            } else if (command.equalsIgnoreCase("delete")) {
                // delete
                deleteTask();
            }
        }
        return false;
    }

    private void deleteTask() {
        System.out.println("Give Location where Task can be deleted");
        String location = kbd.nextLine();
        taskService.deleteTaskFromDB(location);
    }

    private void updateTask() {
    }

    private void readTask() {
        System.out.println("Give Name Location to see Active Tasks OR input 'All'");
        String input = kbd.nextLine();
        if (input.equalsIgnoreCase("all")) {
            taskService.readTaskFromDB();
        } else if (!input.equalsIgnoreCase("all"))
            taskService.readTaskFromDB(input);
    }

    private void saveTask() {
        System.out.println("Give Description of task: ");
        String task = kbd.nextLine();
        Task object = taskService.saveTaskToDB(new Task(task));
        System.out.println("Give place");
        String place = kbd.nextLine();
        taskService.addTaskToLocation(place, object);
    }


    // LOCATION << Functions >>
    private boolean locationConditions(String command) {
        showMenuLocation();
        command = kbd.nextLine();
        if (command.equalsIgnoreCase("quit")) {
            System.out.println("GoodBye my Builder Lord");
            em.close();
            em.getEntityManagerFactory().close();
            return true;
        } else if (command.equalsIgnoreCase("save")) {
            // save
            saveLocation();
        } else if (command.equalsIgnoreCase("read")) {
            // read
            readLocation();
        } else if (command.equalsIgnoreCase("update")) {
            // update
            updateLocation();
        } else if (command.equalsIgnoreCase("delete")) {
            // delete
            deleteLocation();
        }
        return false;
    }

    private void deleteLocation() {
        System.out.println("Give Location name to delete:");
        String name = kbd.nextLine();
        locationService.deleteLocationFromDB(name);
    }

    private void updateLocation() {
        System.out.println("Give Name of location to update: ");
        String name = kbd.nextLine();
        System.out.println("Give new Name of location: ");
        String newName = kbd.nextLine();
        locationService.updateLocationFromDB(name, newName);
    }

    private void readLocation() {
        System.out.println("Give Name Location to print Location and Tasks");
        System.out.println("OR press Enter for Printing all Locations: ");
        String location = kbd.nextLine();
        if (location.equalsIgnoreCase("all")) {
            locationService.readLocationFromDB();
        } else if (!location.equalsIgnoreCase("all")) {
            locationService.readLocationFromDB(location);
        }
    }

    private void saveLocation() {
        System.out.println("Give name of new Location: ");
        String place = kbd.nextLine();
        locationService.saveLocationToDB(new Location(place));
    }


    // EMP << Functions >>      DONE
    private boolean employeeConditions(String command) {
        if (command.equalsIgnoreCase("quit")) {
            System.out.println("GoodBye my Builder Lord");
            em.close();
            em.getEntityManagerFactory().close();
            return true;
        } else if (command.equalsIgnoreCase("save")) {
            // save
            SaveEmployee();
        } else if (command.equalsIgnoreCase("read")) {
            // read
            readEmployee();
        } else if (command.equalsIgnoreCase("update")) {
            // update
            addTask();
        } else if (command.equalsIgnoreCase("add")) {

            // update

        } else if (command.equalsIgnoreCase("delete")) {
            // delete
            deleteEmployee();
        } else if (command.equalsIgnoreCase("sick")) {
            //task
            sickDaysAdd();
        }
        return false;
    }

    private void sickDaysAdd() {
        System.out.println("Give EMP last name");
        String lastname = kbd.nextLine();
        System.out.println("Input days of Sickness one or more");
        int days = kbd.nextInt();
        if (days == 1) {
            employeeServcie.updateSickDaysEmp(lastname);
        } else {
            employeeServcie.updateSickDaysEmp(lastname, days);
        }
    }

    private void addTask() {
        System.out.println("Give Last name EMP to add task");
        String lastName = kbd.nextLine();
        System.out.println("Give TaskName: ");
        String taskName = kbd.nextLine();
        employeeServcie.addTask(lastName, taskName);
    }

    private void SaveEmployee() {
        saveEMP();
    }

    private void readEmployee() {
        System.out.println("Give:\n 1 -> for LastName\n 2 -> for FirstName\n 3 -> ALL ");
        int x = kbd.nextInt();
        System.out.println("Give parameter to search: ");
        kbd.nextLine();
        String parameter = kbd.nextLine();
        employeeServcie.readEmployeeToDB(x, parameter);
    }

    private void updateEmployee() {
        System.out.println("Give LastName for EMP to be updated");
        String name = kbd.nextLine();
        System.out.println("What needs to be updated? \n 1 -> Address\n 2 -> city");
        int x = kbd.nextInt();
        kbd.nextLine();
        System.out.println("Input new Data:");
        String parameter = kbd.nextLine();
        employeeServcie.updateEmployeeFromDB(name, x, parameter);
    }

    private void deleteEmployee() {
        System.out.println("Give Last Name to  delete:");
        String name = kbd.nextLine();
        employeeServcie.deleteEmployeeFromDB(name);
    }


    // Private Helpers for << save >>       DONE
    private void saveEMP() {
        employeeServcie.saveEmployeeToDB(inputDetailsEMP(), makeNewContract(), new EmployeeWorkDetails());
    }

    private Employee inputDetailsEMP() {
        System.out.println("Give First Name: ");
        String fName = kbd.nextLine();
        System.out.println("Give Last Name: ");
        String lName = kbd.nextLine();
        System.out.println("Give Phone number: ");
        String phone = kbd.nextLine();
        System.out.println("Give Address: ");
        String address = kbd.nextLine();
        System.out.println("Give City: ");
        String city = kbd.nextLine();
        Employee employee = new Employee(fName, lName, phone, address, city, inputBday(kbd));

        return employee;

    }

    private LocalDate inputBday(Scanner kbd) {
        System.out.println("Give Birthday: ");
        System.out.println("YYYY");
        int y = kbd.nextInt();
        System.out.println("MM");
        int m = kbd.nextInt();
        System.out.println("DD");
        int d = kbd.nextInt() + 1;
        return LocalDate.of(y, m, d);
    }

    private Contract makeNewContract() {
        System.out.println("Give StartUp Salary: ");
        double salary = kbd.nextDouble();
        System.out.println("Give Contract type: Fix or Freelance");
        Contract contract;
        kbd.nextLine();
        String cType = kbd.nextLine();
        if (cType.equalsIgnoreCase("fix")) {
            Contract.ContractType fix = Contract.ContractType.CONTRACT;
            contract = new Contract(salary, fix);
        } else {
            Contract.ContractType free = Contract.ContractType.FREELANCE;
            contract = new Contract(salary, free);
        }
        return contract;
    }

}
