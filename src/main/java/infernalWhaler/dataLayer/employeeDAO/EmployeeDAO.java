package infernalWhaler.dataLayer.employeeDAO;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.employee.Contract;
import infernalWhaler.model.employee.Employee;
import infernalWhaler.model.employee.EmployeeWorkDetails;
import infernalWhaler.model.project.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents dataLayer of Employee class
 * @see Employee
 * @see infernalWhaler.service.employeeServcie.EmployeeServcie
 */
public class EmployeeDAO {

    private EntityManager em = EmProvidor.getEntityManager();
    private EntityTransaction et = em.getTransaction();


    /**
     * Saves a new employee to the Database
     */
    public void saveEmployee(Employee employee, Contract contract, EmployeeWorkDetails employeeWorkDetails) {
        System.out.println("Connecting to the database....");
        et.begin();
        employee.addContract(contract);
        employeeWorkDetails.addEmployee(employee);
        em.persist(employeeWorkDetails);
        et.commit();
    }


    /**
     * Read employees from the Database
     * @param x to loop through switch
     * @param indicator
     */
    public void readEMP(int x, String indicator) {
        System.out.println("Connection to database...");
        et.begin();
        TypedQuery<Employee> query;
        List<Employee> employees = null;
        switch (x) {
            case 1:
                query = getEmpLastNameTypedQuery(indicator);
                employees = query.getResultList();
                break;
            case 2:
                query = getEmpFirstNameTypedQuery(indicator);
                employees = query.getResultList();
                break;
            case 3:
                query = getAllTypedQuery();
                employees = query.getResultList();
        }
        System.out.println(employees);
        et.commit();
    }

    /** Removes an Employee from the Database
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#deleteEmployeeFromDB(String)  */
    public void removeEmployee(Employee employee) {
        System.out.println("deleting " + employee);
        et.begin();
        em.remove(employee);
        et.commit();
    }

    /** Updates details of an Employe in the database
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#updateEmployeeFromDB(String, int, String) */
    public void updateEmployee(Employee employee) {
        System.out.println("Updating employee" + employee);
        et.begin();
        em.persist(employee);
        et.commit();
    }

    /** Method Overloading:
     * Updates details of an Employe via class EmployeeWorkDetails in the database
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#updateEmployeeFromDB(String, int, String)
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#updateSickDaysEmp(String)
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#updateSickDaysEmp(String, int) */
    public void updateEmployee(EmployeeWorkDetails emp){
        System.out.println("Updating employee" + emp);
        et.begin();
        em.persist(emp);
        et.commit();
    }


    /**
     * Add Tasks to a specific Employee and adds this Employee to same specific Task
     * Searches an employee by name and adds a task to it
     * @param name
     * @param  task is at same time added to employee
     * @see infernalWhaler.service.employeeServcie.EmployeeServcie#addTask(String, String)
     */
    public void addEmpToTask(String name, Task task) {
        System.out.println("Connecting to database...");
        et.begin();
        TypedQuery<EmployeeWorkDetails> query = getEmpLastNameViaWorkDetails(name);
        List<EmployeeWorkDetails> employees = query.getResultList();
        for (EmployeeWorkDetails employee : employees) {
            employee.addTasks(task);
            task.addEmployeeWorkDetails(employee);
            em.merge(employee);
        }
        et.commit();
    }


    /**
     * @return query of all employees
     */
    public TypedQuery<Employee> getAllTypedQuery() {
        TypedQuery<Employee> query = em.createQuery("select e from Employee as e", Employee.class);
        return query;
    }

    /**
     * @return a query of employee with @para lastName
     */
    public TypedQuery<Employee> getEmpLastNameTypedQuery(String lastName) {
        TypedQuery<Employee> query = em.createQuery("select e from Employee as e where e.lastName=:last", Employee.class);
        query.setParameter("last", lastName);
        return query;
    }

    /**
     * @return a query of employee with @para firstName
     */
    public TypedQuery<Employee> getEmpFirstNameTypedQuery(String firstName) {
        TypedQuery<Employee> query = em.createQuery("select e from Employee as e where e.firstName=:last", Employee.class);
        query.setParameter("last", firstName);
        return query;
    }

    /**
     * @return a query employeeWorkDetails via Employee with @para lastName
     */
    public TypedQuery<EmployeeWorkDetails> getEmpLastNameViaWorkDetails(String lastName) {
        TypedQuery<EmployeeWorkDetails> query = em.createQuery("select e from EmployeeWorkDetails as e where e.employee.lastName=:last", EmployeeWorkDetails.class);
        query.setParameter("last", lastName);
        return query;
    }

}

