package infernalWhaler.dataLayer.projectDAO;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.project.Location;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the dataLayer of Location class
 *
 * @see Location
 * @see infernalWhaler.service.projectService.LocationService
 */
public class LocationDAO {

    private EntityManager em = EmProvidor.getEntityManager();
    private EntityTransaction et = em.getTransaction();


    /**
     * Saves a Location to the database
     */
    public Location saveLocation(Location location) {
        System.out.println("Importing new Location ");
        et.begin();
        em.persist(location);
        et.commit();
        return location;
    }


    /**
     * Reads all Locations from the database
     */
    public void readLocation() {
        System.out.println("Connection to Database...");
        et.begin();
        TypedQuery<Location> query = getLocationQuery();
        List<Location> locations = query.getResultList();
        for (Location location : locations) {
            System.out.println(location);
        }
        et.commit();
    }

    /**
     * Reads a specific Location, where @para indicator is location name, from the database
     */
    public void readLocation(String indicator) {
        System.out.println("Connection to Database...");
        et.begin();
        TypedQuery<Location> query = getLocationNameQuery(indicator);
        List<Location> locations = query.getResultList();
        for (Location location : locations) {
            System.out.println(location.getLocation() + ": " + location.getTasks());
        }
        et.commit();
    }


    /**
     * Removes a Location from the database
     */
    public void removeLocation(Location location) {
        System.out.println("Deleting " + location);
        et.begin();
        em.remove(location);
        et.commit();
    }



    /**
     * @return all locations
     */
    public TypedQuery<Location> getLocationQuery() {
        TypedQuery<Location> query = em.createQuery("select l from Location as l", Location.class);
        return query;
    }

    /**
     * @return specific location by name
     */
    public TypedQuery<Location> getLocationNameQuery(String place) {
        TypedQuery<Location> query = em.createQuery("select l from Location  as l where l.location=:name", Location.class);
        query.setParameter("name", place);
        return query;
    }


}
