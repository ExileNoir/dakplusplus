package infernalWhaler.dataLayer.projectDAO;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.project.ServiceProducts;
import infernalWhaler.model.project.Task;
import infernalWhaler.service.projectService.ServiceProductsService;


import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the dataLayer of the ServiceProducts class
 *
 * @see infernalWhaler.model.project.ServiceProducts
 * @see infernalWhaler.service.projectService.ServiceProductsService
 */
public class ServiceProductsDAO {

    private EntityManager em = EmProvidor.getEntityManager();
    private EntityTransaction et = em.getTransaction();

    /**
     * Saves ServiceProducts to the database
     *
     * @see ServiceProductsService#saveServiceProductsToDB(ServiceProducts)
     */
    public void saveServiceProducts(ServiceProducts sp) {
        System.out.println("Connecting to the database....");
        et.begin();
        em.persist(sp);
        et.commit();
    }

    // Read

    // Remove


    /**
     * Updates ServiceProducts to database
     *
     * @param sp to update an object of ServiceProducts
     * @see ServiceProductsService#updateServiceProductsFromDB(long, String, double)
     */
    public void updateServiceProducts(ServiceProducts sp) {
        System.out.println("Updating Service Product " + sp);
        et.begin();
        em.persist(sp);
        et.commit();
    }

    /**
     * Add ServiceProducts to specific Task and adds this Task to the same specific ServiceProduct
     *
     * @param idsp Searches a ServiceProduct by ID and adds a Task to it
     * @param task to add the Task to specific ServiceProduct
     */
    public void addServiceProductsToTask(long idsp, Task task) {
        System.out.println("Connecting to the database...");
        et.begin();
        TypedQuery<ServiceProducts> query = getServiceProductByIdQuery(idsp);
        List<ServiceProducts> serviceProducts = query.getResultList();
        for (ServiceProducts sp : serviceProducts) {
            sp.addTask(task);
            task.addMaterials(sp);
            em.merge(sp);
        }
        et.commit();
    }


    /**
     * @return a query of all ServiceProducts
     */
    public TypedQuery<ServiceProducts> getAllServiceProducts() {
        TypedQuery<ServiceProducts> query = em.createQuery("select sp from ServiceProducts as sp", ServiceProducts.class);
        return query;
    }

    /**
     * @return a query of ServiceProducts with specific ID
     */
    public TypedQuery<ServiceProducts> getServiceProductByIdQuery(long id) {
        TypedQuery<ServiceProducts> query = em.createQuery("select sp from ServiceProducts as sp where sp.id=:input", ServiceProducts.class);
        query.setParameter("input", id);
        return query;
    }


}
