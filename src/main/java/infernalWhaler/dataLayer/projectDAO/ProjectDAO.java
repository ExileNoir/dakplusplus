package infernalWhaler.dataLayer.projectDAO;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.project.Project;
import infernalWhaler.model.project.Task;

import javax.persistence.*;
import java.util.List;

/**
 * Represents the dataLayer of Project class
 *
 * @see Project
 * @see infernalWhaler.service.projectService.ProjectService
 */
public class ProjectDAO {


    private EntityManager em = EmProvidor.getEntityManager();
    private EntityTransaction et = em.getTransaction();


    /**
     * Saves a new Project to the database
     */
    public void saveProject(Project project) {
        System.out.println("Importing new Project ");
        et.begin();
        em.persist(project);
        et.commit();
    }

    /**
     * Reads all Projects from the database
     */
    public void readProject() {
        System.out.println("Connection to database");
        et.begin();
        TypedQuery<Project> query = getProjectQuery();
        List<Project> projects = query.getResultList();
        for (Project project : projects) {
            System.out.println("Project Name: " + project.getName() + " Tasks: " + project.getTasks());
        }
    }

    /**
     * @param projectName queries for specific Project on name
     */
    public void readProject(String projectName) {
        System.out.println("Connection to Database...");
        et.begin();
        TypedQuery<Project> query = getProjectByNameQuery(projectName);
        List<Project> projects = query.getResultList();
        for (Project project : projects) {
            System.out.println("Project Name: " + project.getName() + " Tasks: " + project.getTasks());
        }
    }

    /**
     * Deletes a Project from the database
     *
     * @param projectName to query searches a specific Project on name
     */
    public void deleteProject(String projectName) {
        System.out.println("Connection to Database...");
        et.begin();
        TypedQuery<Project> query = getProjectByNameQuery(projectName);
        List<Project> projects = query.getResultList();
        for (Project project : projects) {
            em.remove(project);
        }
        et.commit();
    }


    /**
     * @param name to query search on Project name
     * @param task to add a task to the project
     * @see infernalWhaler.service.projectService.ProjectService#addTaskToProject(String, String)
     */
    public void addTaskToProject(String name, Task task) {
        System.out.println("Connection to database...");
        et.begin();
        TypedQuery<Project> query = getProjectByNameQuery(name);
        List<Project> projects = query.getResultList();
        for (Project project : projects) {
            project.addTasks(task);
            em.merge(project);
        }
        et.commit();
    }


    /**
     * @return query of all Projects
     */
    public TypedQuery<Project> getProjectQuery() {
        TypedQuery<Project> query = em.createQuery("select p from Project as p", Project.class);
        return query;
    }

    /**
     * @return query Project by specific name
     */
    public TypedQuery<Project> getProjectByNameQuery(String projectName) {
        TypedQuery<Project> query = em.createQuery("select p from Project as p where p.name=:name", Project.class);
        query.setParameter("name", projectName);
        return query;
    }
}
