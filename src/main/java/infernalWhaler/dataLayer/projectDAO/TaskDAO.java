package infernalWhaler.dataLayer.projectDAO;

import infernalWhaler.dataLayer.EmProvidor;
import infernalWhaler.model.project.Task;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents dataLayer of Task class
 *
 * @see Task
 * @see infernalWhaler.service.projectService.TaskService
 */
public class TaskDAO {


    private EntityManager em = EmProvidor.getEntityManager();
    private EntityTransaction et = em.getTransaction();


    /**
     * saves a Task to the Database
     */
    public void saveTask(Task task) {
        System.out.println("Inporting new Task");
        et.begin();
        em.persist(task);
        et.commit();
    }


    /**
     * reads all Tasks from the database
     */
    public void readTask() {
        System.out.println("Connecting to database...");
        et.begin();
        TypedQuery<Task> query = getTaskQuery();
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            System.out.println(task);
        }
        et.commit();
    }

    /**
     * @param location to get task by location name
     *                 reads a specific Task from the database
     */
    public void readTask(String location) {
        System.out.println("Connecting to database...");
        et.begin();
        TypedQuery<Task> query = getTaskByLocationQuery(location);
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            System.out.println(task.getNameTask());
        }
        et.commit();
    }


    /**
     * Deletes a specific Task from the database
     */
    public void deleteTask(String location) {
        System.out.println("Deleting ");
        et.begin();
        TypedQuery<Task> query = getTaskByLocationQuery(location);
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            em.remove(task);
        }
        et.commit();
    }


    /**
     * @return all Tasks
     */
    public TypedQuery<Task> getTaskQuery() {
        TypedQuery<Task> query = em.createQuery("select t from Task as t", Task.class);
        return query;
    }

    /**
     * @return all Tasks with specific Location
     */
    public TypedQuery<Task> getTaskByLocationQuery(String name) {
        TypedQuery<Task> query = em.createQuery("select t from Task as t where t.location.location=:name", Task.class);
        query.setParameter("name", name);
        return query;
    }

    /**
     * @return specific Task by name
     */
    public TypedQuery<Task> getTaskByName(String nametask) {
        TypedQuery<Task> query = em.createQuery("select t from Task as t where t.nameTask=:name", Task.class);
        query.setParameter("name", nametask);
        return query;
    }

}
