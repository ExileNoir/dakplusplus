package infernalWhaler.dataLayer;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Date;

/**
 * Singleton EntityManagerFactory && EntityManager
 */
public class EmProvidor {

    private static final String DB_PU = "DakPlusPlus";

    public static final boolean DEBUG = true;

    private static EmProvidor ourInstance = new EmProvidor();

    private static EntityManagerFactory emf;

    private EntityManager em;

    private EmProvidor() {
    }

    public static EmProvidor getInstance() {
        return ourInstance;
    }

    /** Creates an EntityManagerFactory */
    public static EntityManagerFactory getEntityManagerFactory() {
        if (emf == null) {
            emf = Persistence.createEntityManagerFactory(DB_PU);
        }
        if (DEBUG) {
            System.out.println("Factory Created on " + new Date());
        }
        return emf;
    }

    /** Creates an EntityManager via:
     * @see EmProvidor#getEntityManagerFactory() */
    public static EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }

    public void closeEmf() {
        if (emf.isOpen() || emf != null) {
            emf.close();
        }
        emf = null;
        if (DEBUG) {
            System.out.println("EMF closed at: " + new Date());
        }
    }


}
