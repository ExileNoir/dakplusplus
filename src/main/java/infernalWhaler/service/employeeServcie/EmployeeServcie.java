package infernalWhaler.service.employeeServcie;

import infernalWhaler.dataLayer.employeeDAO.EmployeeDAO;
import infernalWhaler.dataLayer.projectDAO.TaskDAO;
import infernalWhaler.model.employee.Contract;
import infernalWhaler.model.employee.Employee;
import infernalWhaler.model.employee.EmployeeWorkDetails;
import infernalWhaler.model.project.Task;

import javax.persistence.TypedQuery;

import java.util.List;

/**
 * Represents the serviceLayer of the Employee class
 *
 * @see Employee
 * @see EmployeeDAO
 */
public class EmployeeServcie {

    private EmployeeDAO employeeDAO;
    private TaskDAO taskDAO;


    public EmployeeServcie() {
        employeeDAO = new EmployeeDAO();
        taskDAO = new TaskDAO();
    }


    /**
     * @see EmployeeDAO#saveEmployee(Employee, Contract, EmployeeWorkDetails)
     */
    public void saveEmployeeToDB(Employee employee, Contract contract, EmployeeWorkDetails employeeWorkDetails) {
        employeeDAO.saveEmployee(employee, contract, employeeWorkDetails);
    }

    /**
     * @see EmployeeDAO#readEMP(int, String)
     */
    public void readEmployeeToDB(int x, String name) {
        employeeDAO.readEMP(x, name);
    }

    /**
     * @param name queries for specific Employee on Last Name
     * @see EmployeeDAO#getEmpLastNameTypedQuery(String)
     * @see EmployeeDAO#removeEmployee(Employee)
     */
    public void deleteEmployeeFromDB(String name) {
        TypedQuery<Employee> query = employeeDAO.getEmpLastNameTypedQuery(name);
        List<Employee> employees = query.getResultList();
        for (Employee employee : employees) {
            employeeDAO.removeEmployee(employee);
        }
    }


    /**
     * Updates the Employee
     *
     * @param name      to select a specific Employee
     * @param x         to go through switch
     * @param parameter input needed for update
     * @see EmployeeDAO#updateEmployee(Employee)
     * @see EmployeeDAO#getEmpLastNameTypedQuery(String)
     */
    public void updateEmployeeFromDB(String name, int x, String parameter) {
        TypedQuery<Employee> query = employeeDAO.getEmpLastNameTypedQuery(name);
        List<Employee> employees = query.getResultList();
        for (Employee e : employees) {
            switch (x) {
                case 1:
                    e.setAddress(parameter);
                    break;
                case 2:
                    e.setCity(parameter);
                    break;
            }
            employeeDAO.updateEmployee(e);
        }
    }

    /**
     * Adds one sick day to and Employee
     *
     * @param lastName to query search on lastName of a specific Employee via EmployeeWorkDetails
     * @see EmployeeDAO#getEmpLastNameViaWorkDetails(String)
     * @see EmployeeDAO#updateEmployee(EmployeeWorkDetails)
     */
    public void updateSickDaysEmp(String lastName) {
        TypedQuery<EmployeeWorkDetails> query = employeeDAO.getEmpLastNameViaWorkDetails(lastName);
        List<EmployeeWorkDetails> emps = query.getResultList();
        for (EmployeeWorkDetails emp : emps) {
            emp.addOneSickDay();
            employeeDAO.updateEmployee(emp);
        }
    }

    /**
     * Methods Overloading:
     * Possibility to add more sick days at once
     */
    public void updateSickDaysEmp(String lastName, int x) {
        TypedQuery<EmployeeWorkDetails> query = employeeDAO.getEmpLastNameViaWorkDetails(lastName);
        List<EmployeeWorkDetails> emps = query.getResultList();
        for (EmployeeWorkDetails emp : emps) {
            emp.setSickDays(emp.getSickDays() + x);
            employeeDAO.updateEmployee(emp);

        }
    }


    /**
     * Adds a Task to an Employee
     *
     * @param empName  for previous method to query the specific Employee by name
     * @param taskName to query search a specific Task name
     * @see TaskDAO#getTaskByName(String)
     * @see EmployeeDAO#addEmpToTask(String, Task)
     */
    public void addTask(String empName, String taskName) {
        TypedQuery<Task> query = taskDAO.getTaskByName(taskName);
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            employeeDAO.addEmpToTask(empName, task);
        }
    }


}

