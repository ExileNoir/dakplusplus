package infernalWhaler.service.projectService;

import infernalWhaler.dataLayer.projectDAO.LocationDAO;
import infernalWhaler.model.project.Location;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the ServiceLayer of Location class
 *
 * @see Location
 * @see LocationDAO
 */
public class LocationService {


    private LocationDAO locationDAO;


    public LocationService() {
        locationDAO = new LocationDAO();
    }


    /**
     * @see LocationDAO#saveLocation(Location)
     */
    public void saveLocationToDB(Location location) {
        locationDAO.saveLocation(location);
    }

    /**
     * @see LocationDAO#readLocation()
     */
    public void readLocationFromDB() {
        locationDAO.readLocation();
    }

    /**
     * @see LocationDAO#readLocation(String)
     */
    public void readLocationFromDB(String indicator) {
        locationDAO.readLocation(indicator);
    }


    /**
     * Removes a specific location
     *
     * @param name queries for a specific Location and removes
     * @see LocationDAO#removeLocation(Location)
     */
    public void deleteLocationFromDB(String name) {
        TypedQuery<Location> query = locationDAO.getLocationNameQuery(name);
        List<Location> locations = query.getResultList();
        for (Location location : locations) {
            locationDAO.removeLocation(location);
        }
    }

    /**
     * Updates a Location from the database
     *
     * @param name    queries for a specific Location
     * @param newName sets now Location name
     * @see LocationDAO#saveLocation(Location)
     */
    public void updateLocationFromDB(String name, String newName) {
        TypedQuery<Location> query = locationDAO.getLocationNameQuery(name);
        List<Location> locations = query.getResultList();
        for (Location location : locations) {
            location.setLocation(newName);
            locationDAO.saveLocation(location);
        }
    }

}
