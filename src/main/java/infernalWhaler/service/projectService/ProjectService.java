package infernalWhaler.service.projectService;


import infernalWhaler.dataLayer.projectDAO.ProjectDAO;
import infernalWhaler.dataLayer.projectDAO.TaskDAO;
import infernalWhaler.model.project.Project;
import infernalWhaler.model.project.Task;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the serviceLayer of Project class
 *
 * @see ProjectDAO
 * @see Project
 */
public class ProjectService {


    private ProjectDAO projectDAO;
    private TaskDAO taskDAO;


    public ProjectService() {
        projectDAO = new ProjectDAO();
        taskDAO = new TaskDAO();
    }


    /**
     * @see ProjectDAO#saveProject(Project)
     */
    public void saveProjectToDB(Project project) {
        projectDAO.saveProject(project);
    }

    /**
     * @see ProjectDAO#readProject()
     */
    public void readProjectFromDB() {
        projectDAO.readProject();
    }

    /**
     * @see ProjectDAO#readProject(String)
     */
    public void readProjectFromDB(String indicator) {
        projectDAO.readProject(indicator);
    }

    public void deleteProjectFromDB(String projectName) {
        projectDAO.deleteProject(projectName);
    }

    /**
     * @param projectName to start the search of Project name
     * @param taskName    to search for specific Task by query
     * @see infernalWhaler.dataLayer.projectDAO.ProjectDAO#addTaskToProject(String, Task)
     * @see infernalWhaler.dataLayer.projectDAO.TaskDAO#getTaskByName(String)
     */
    public void addTaskToProject(String projectName, String taskName) {
        TypedQuery<Task> query = taskDAO.getTaskByName(taskName);
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            projectDAO.addTaskToProject(projectName, task);
        }

    }
}
