package infernalWhaler.service.projectService;

import infernalWhaler.dataLayer.projectDAO.ServiceProductsDAO;
import infernalWhaler.dataLayer.projectDAO.TaskDAO;
import infernalWhaler.model.project.ServiceProducts;
import infernalWhaler.model.project.Task;


import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the serviceLayer of ServiceProducts class
 *
 * @see infernalWhaler.model.project.ServiceProducts
 * @see infernalWhaler.dataLayer.projectDAO.ServiceProductsDAO
 */
public class ServiceProductsService {

    private ServiceProductsDAO serviceProductsDAO;
    private TaskDAO taskDAO;

    public ServiceProductsService() {
        this.serviceProductsDAO = new ServiceProductsDAO();
        this.taskDAO = new TaskDAO();
    }

    /**
     * @see ServiceProductsDAO#saveServiceProducts(ServiceProducts)
     */
    public void saveServiceProductsToDB(ServiceProducts sp) {
        serviceProductsDAO.saveServiceProducts(sp);
    }

    /**
     * Updates The Service Products: Price per Materials, Price per Hour, Hours of work
     *
     * @param spId         query search on Id of specific ServiceProduct
     * @param whatToChange to start the switch case
     * @param amount       for the value of what needs to be changed
     * @see ServiceProductsDAO#getServiceProductByIdQuery(long)
     * @see ServiceProductsDAO#updateServiceProducts(ServiceProducts)
     */
    public void updateServiceProductsFromDB(long spId, String whatToChange, double amount) {
        TypedQuery<ServiceProducts> query = serviceProductsDAO.getServiceProductByIdQuery(spId);
        List<ServiceProducts> serviceProducts = query.getResultList();
        for (ServiceProducts sp : serviceProducts) {
            switch (whatToChange) {
                case "Price Materials":
                    System.out.println("Add to existing material price: ");
                    sp.setPriceMaterials(sp.getPriceMaterials() + amount);
                    break;
                case "Hours Workers":
                    System.out.println("Add extra hours:");
                    sp.setHoursWorker(sp.getHoursWorker() + amount);
                    break;
                case "Price Hours":
                    System.out.println("Give new Price per Hour for workers");
                    sp.setPricePerHour(amount);
                    break;
            }
            serviceProductsDAO.updateServiceProducts(sp);
        }
    }

    /**
     * Adds a Task to a ServiceProduct
     *
     * @param spId     for previous method to query the specific ServiceProduct by ID
     * @param taskName to query search a specific Task by name
     * @see TaskDAO#getTaskByName(String)
     * @see ServiceProductsDAO#addServiceProductsToTask(long, Task)
     */
    public void addTaskToServiceProducts(long spId, String taskName) {
        TypedQuery<Task> query = taskDAO.getTaskByName(taskName);
        List<Task> tasks = query.getResultList();
        for (Task task : tasks) {
            serviceProductsDAO.addServiceProductsToTask(spId, task);
        }
    }
}
