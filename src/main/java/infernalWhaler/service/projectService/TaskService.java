package infernalWhaler.service.projectService;

import infernalWhaler.dataLayer.projectDAO.LocationDAO;
import infernalWhaler.dataLayer.projectDAO.TaskDAO;
import infernalWhaler.model.project.Location;
import infernalWhaler.model.project.Task;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Represents the ServiceLayer of the Task class
 *
 * @see Task
 * @see TaskDAO
 */
public class TaskService {

    private TaskDAO taskDAO;
    private LocationDAO locationDAO;


    public TaskService() {
        taskDAO = new TaskDAO();
        locationDAO = new LocationDAO();
    }


    /**
     * Saves a Task
     *
     * @see TaskDAO#saveTask(Task)
     */
    public Task saveTaskToDB(Task task) {
        taskDAO.saveTask(task);
        return task;
    }

    /**
     * Adds a specific Task to a specific Location
     *
     * @param name queries for a specific Location by name
     * @param task sets a Task to the specific location
     * @see TaskService#saveTaskToDB(Task)
     */
    public void addTaskToLocation(String name, Task task) {
        TypedQuery<Location> query = locationDAO.getLocationNameQuery(name);
        List<Location> locations = query.getResultList();
        for (Location location : locations) {
            location.addTask(task);
            saveTaskToDB(task);

        }
    }

    /**
     * @see TaskDAO#readTask()
     */
    public void readTaskFromDB() {
        taskDAO.readTask();
    }

    /**
     * @see TaskDAO#readTask(String)
     */
    public void readTaskFromDB(String location) {
        taskDAO.readTask(location);
    }

    /**
     * @see TaskDAO#deleteTask(String)
     */
    public void deleteTaskFromDB(String location) {
        taskDAO.deleteTask(location);
    }

}
