package infernalWhaler.model.project;


import infernalWhaler.model.client.Customer;
import infernalWhaler.model.client.Invoice;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Project
 *
 * @see infernalWhaler.dataLayer.projectDAO.ProjectDAO
 * @see infernalWhaler.service.projectService.ProjectService
 */
@Entity
public class Project {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "Project_Name")
    private String name;


    @OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Task> tasks;

    @OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Invoice> invoices;

    @ManyToMany(mappedBy = "projects", cascade = {CascadeType.ALL})
    private Set<Customer> customers;

    @OneToMany(mappedBy = "project", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Quote> quotes;

    // Constructor
    public Project() {
        this.tasks = new HashSet<>();
        this.invoices = new HashSet<>();
        this.customers = new HashSet<>();
        this.quotes = new HashSet<>();
    }

    public Project(String name) {
        this.name = name;
        this.tasks = new HashSet<>();
        this.invoices = new HashSet<>();
        this.customers = new HashSet<>();
        this.quotes = new HashSet<>();
    }

    // Methods
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTasks(Task task) {
        if (tasks != null) {
            tasks.add(task);
            task.setProject(this);
        }
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }

    public void addInvoices(Invoice invoice) {
        if (invoices != null) {
            invoices.add(invoice);
            invoice.setProject(this);
        }
    }

    public Set<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(Set<Customer> customers) {
        this.customers = customers;
    }

    public void AddCustomers(Customer customer) {
        if (customers != null) {
            customers.add(customer);
        }
    }

    public Set<Quote> getQuotes() {
        return quotes;
    }

    public void setQuotes(Set<Quote> quotes) {
        this.quotes = quotes;
    }

    public void addQuotes(Quote quote) {
        if (quotes != null) {
            quotes.add(quote);
            quote.setProject(this);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Project: " + getName() + "\nTasks:\n");
        for (Task task : tasks) {
            sb.append(task.getNameTask()).append("  ").append(" --> ").append(task.getLocation()).append("\n");
        }
        for (Invoice invoice : invoices) {
            sb.append(invoice.getId()).append(" Amount: ").append(invoice.getPrice()).append("\n");
        }


        return sb.toString();
    }
}
