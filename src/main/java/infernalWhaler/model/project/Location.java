package infernalWhaler.model.project;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a Location
 *
 * @see infernalWhaler.dataLayer.projectDAO.LocationDAO
 * @see infernalWhaler.service.projectService.LocationService
 */
@Entity
public class Location {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Location")
    private long id;

    @Column(name = "Location")
    private String location;

    @OneToMany(mappedBy = "location", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<Task> tasks;


    // Constructor
    public Location() {
        this.tasks = new ArrayList<>();
    }

    public Location(String location) {
        this.location = location;
        this.tasks = new ArrayList<>();
    }

    // Methods
    public long getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void addTask(Task task) {
        if (tasks != null) {
            tasks.add(task);
            task.setLocation(this);
        }
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Location: " + getLocation() + "\nTasks: ");
        for (Task task : tasks) {
            sb.append(task.getNameTask()).append("\n");
        }
        return sb.toString();
    }

}
