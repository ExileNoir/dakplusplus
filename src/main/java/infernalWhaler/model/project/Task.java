package infernalWhaler.model.project;

import infernalWhaler.model.employee.EmployeeWorkDetails;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Task
 *
 * @see infernalWhaler.dataLayer.projectDAO.TaskDAO
 * @see infernalWhaler.service.projectService.TaskService
 */
@Entity
@Table(name = "Task")
public class Task {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_task")
    private long id;

    @Column(name = "Task")
    private String nameTask;

    @ManyToMany(mappedBy = "tasks", cascade = {CascadeType.ALL})
    private Set<EmployeeWorkDetails> employeeWorkDetails;

    @ManyToOne
    @JoinColumn(name = "Location_ID")
    private Location location;

    @ManyToOne
    @JoinColumn(name = "Project_id")
    private Project project;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "Task_VS_Materials",
            joinColumns = {@JoinColumn(name = "Materials_ID")},
            inverseJoinColumns = {@JoinColumn(name = "Task_ID")})
    private Set<ServiceProducts> materials;

    // Constructor
    public Task() {
        this.employeeWorkDetails = new HashSet<>();
        this.materials = new HashSet<>();
    }

    public Task(String nameTask) {
        this.nameTask = nameTask;
        this.employeeWorkDetails = new HashSet<>();
        this.materials = new HashSet<>();
    }

    // Methods
    public void setId(long id) {
        this.id = id;
    }

    public String getNameTask() {
        return nameTask;
    }

    public void setNameTask(String nameTask) {
        this.nameTask = nameTask;
    }

    public Set<EmployeeWorkDetails> getEmployeeWorkDetails() {
        return employeeWorkDetails;
    }

    public void setEmployeeWorkDetails(Set<EmployeeWorkDetails> employeeWorkDetails) {
        this.employeeWorkDetails = employeeWorkDetails;
    }

    public void addEmployeeWorkDetails(EmployeeWorkDetails employeeWorkDetail) {
        if (employeeWorkDetails != null) {
            employeeWorkDetails.add(employeeWorkDetail);
        }
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Set<ServiceProducts> getMaterials() {
        return materials;
    }

    public void addMaterials(ServiceProducts serviceProducts) {
        if (materials != null) {
            materials.add(serviceProducts);
        }
    }

    public void setMaterials(Set<ServiceProducts> materials) {
        this.materials = materials;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Task name: " + getNameTask() + "\n Active EMP's: \n ");
        for (EmployeeWorkDetails ewd : employeeWorkDetails) {
            sb.append(ewd.getEmployee().getLastName()).append(" ").append(ewd.getEmployee().getFirstName());
            sb.append(" Location: ").append(getLocation().getLocation()).append(" ");
        }

        for (ServiceProducts mat : materials) {
            sb.append("\nMaterials and Hours: ").append(mat.getMaterialName()).append(" €").append(mat.getPriceMaterials()).append(" Hours: ").append(mat.getHoursWorker()).append(" €").append(mat.getPricePerHour()).append("\nTOTAL: €").append(mat.getTotalPrice());
        }
        return sb.toString();
    }

}
