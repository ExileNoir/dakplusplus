package infernalWhaler.model.project;

import javax.persistence.*;
import java.util.*;

/**
 * Represents the Service and Products of a Project
 * @see infernalWhaler.dataLayer.projectDAO.ServiceProductsDAO
 * @see infernalWhaler.service.projectService.ServiceProductsService
 */
@Entity
@Table(name = "Service_and_Products")
public class ServiceProducts {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Materials")
    private long id;

    @Column(name = "Materials")
    private String materialName;

    @Column(name = "Price_Materials")
    private double priceMaterials;

    @Column(name = "Hours_Worker")
    private double hoursWorker;

    @Column(name = "Price_Hours")
    private double pricePerHour = 50;

    @Column(name = "Price_Total_Hours")
    private double priceTotalHours;

    @Column(name = "Total_Price_Hours_and_Materials")
    private double totalPrice;

    @ManyToMany(mappedBy = "materials", cascade = {CascadeType.ALL})
    private Set<Task> tasks;


    // Constructor
    public ServiceProducts() {
        this.tasks = new HashSet<>();
    }

    public ServiceProducts(String materialName, double priceMaterials, double hoursWorker) {
        this.materialName = materialName;
        this.priceMaterials = priceMaterials;
        this.hoursWorker = hoursWorker;
        this.tasks = new HashSet<>();
        calcPriceTotalHours();
        calcTotalPriceHoursAndMaterials();
    }

    // Methods
    public long getId() {
        return id;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public double getPriceMaterials() {
        return priceMaterials;
    }

    public void setPriceMaterials(double priceMaterials) {
        this.priceMaterials = priceMaterials;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        if (tasks != null) {
            tasks.add(task);
        }
    }

    public double getHoursWorker() {
        return hoursWorker;
    }

    public void setHoursWorker(double hoursWorker) {
        this.hoursWorker = hoursWorker;
    }


    public double getPricePerHour() {
        return pricePerHour;
    }

    public void setPricePerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    public double calcPriceTotalHours() {
        return this.priceTotalHours = this.pricePerHour * this.hoursWorker;
    }


    public double getPriceTotalHours() {
        return priceTotalHours;
    }

    public void setPriceTotalHours(double priceTotalHours) {
        this.priceTotalHours = priceTotalHours;
    }

    public double calcTotalPriceHoursAndMaterials() {
        return this.totalPrice = priceTotalHours + priceMaterials;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Materialname: " + getMaterialName() + "\nPrice: " + getPriceMaterials());
        return sb.toString();
    }
}




