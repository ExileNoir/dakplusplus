package infernalWhaler.model.project;

import javax.persistence.*;

/**
 * Represents a Quote
 */
@Entity
public class Quote {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Quote")
    private long id;

    @ManyToOne
    @JoinColumn(name = "Project_ID")
    private Project project;


    // Constructor

    // Methods

    public long getId() {
        return id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String toString() {
        return "Quote: " + getId() + " Project: " + getProject().getName() + "\nClient: " + getProject();
    }
}
