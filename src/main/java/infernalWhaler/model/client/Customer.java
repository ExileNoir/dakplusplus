package infernalWhaler.model.client;

import infernalWhaler.model.project.Project;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a Customer
 *
 * @see infernalWhaler.dataLayer.clientDAO.CustomerDAO
 */
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Customer")
    private long id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Address")
    private String address;
    @Column(name = "City")
    private String city;

    @ManyToMany
    @JoinTable(name = "Customer_Project",
            joinColumns = {@JoinColumn(name = "Customer_ID")},
            inverseJoinColumns = {@JoinColumn(name = "Project_ID")})
    private Set<Project> projects;


    // Constructor
    public Customer() {
        this.projects = new HashSet<>();
    }

    public Customer(String name, String address, String city) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.projects = new HashSet<>();
    }

    // Methods
    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public void addProjects(Project project) {
        if (projects != null) {
            projects.add(project);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Customer: " + getId() + " " + getName() + "\nProjects: ");
        for (Project project : projects) {
            sb.append(project.getName() + " " + project.getInvoices() + "\n");
        }
        return sb.toString();
    }
}
