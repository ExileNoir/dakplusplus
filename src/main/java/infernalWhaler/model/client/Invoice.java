package infernalWhaler.model.client;

import infernalWhaler.model.project.Project;

import javax.persistence.*;

@Entity
public class Invoice {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_Invoice")
    private long id;

    @Column(name = "Price")
    private double price;

    @ManyToOne
    @JoinColumn(name = "Project_ID")
    private Project project;

    // Constructor
    public Invoice() {
    }

    public Invoice(double price) {
        this.price = price;
    }

    // Methods
    public long getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String toString(){
        return "Invoice no: "+getId()+" for Project: "+getProject().getName()+"\nAMOUNT: "+getPrice();
    }
}
