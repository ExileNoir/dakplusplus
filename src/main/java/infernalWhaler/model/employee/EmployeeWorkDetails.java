package infernalWhaler.model.employee;

import infernalWhaler.model.project.Task;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents the Work details of an Employee
 *
 * @see Employee
 * @see infernalWhaler.dataLayer.employeeDAO.EmployeeDAO
 * @see infernalWhaler.service.employeeServcie.EmployeeServcie
 * @see Employee
 */
@Entity
@Table(name = "EMP_Work_Details")
public class EmployeeWorkDetails {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EMP")
    private long id;

    @Column(name = "Work_days")
    private double workDays = 0;

    @Column(name = "Sick_days")
    private double sickDays = 0;

    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "Employee_ID")
    private Employee employee;

    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(name = "EMP_Tasks",
            joinColumns = {@JoinColumn(name = "EMP_ID")},
            inverseJoinColumns = {@JoinColumn(name = "Task_ID")})
    private Set<Task> tasks;

    // Constructor
    public EmployeeWorkDetails() {
        this.tasks = new HashSet<>();
    }


    // Methods
    public long getId() {
        return id;
    }

    public double getWorkDays() {
        return workDays;
    }

    public void setWorkDays(double workDays) {
        this.workDays = workDays;
    }

    public double addOneSickDay(){
        sickDays++;
        return sickDays;
    }

    public double getSickDays() {
        return sickDays;
    }

    public void setSickDays(double sickDays) {
        this.sickDays = sickDays;
    }


    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void addEmployee(Employee employee) {
        this.employee = employee;
        employee.setEmployeeWorkDetails(this);
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTasks(Task task) {
        if (tasks != null) {
            tasks.add(task);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Working days: " + getWorkDays() + ", Sick Days: " + getSickDays());
        for (Task task : tasks) {
            sb.append(task.getNameTask());
        }
        return sb.toString();
    }
}
