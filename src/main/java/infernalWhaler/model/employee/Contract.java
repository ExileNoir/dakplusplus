package infernalWhaler.model.employee;

import javax.persistence.*;

/**
 * Represents a Contract
 *
 * @see Employee
 * @see EmployeeWorkDetails
 * @see Contract
 */
@Entity
@Table(name = "Contract")
public class Contract {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "Salary")
    private double salary;

    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "Contract_Type")
    private ContractType contractType;

    @OneToOne(mappedBy = "contract", orphanRemoval = true)
    private Employee employee;

    // Constructor
    public Contract() {
    }

    public Contract(double salary, ContractType contractType) {
        this.salary = salary;
        this.contractType = contractType;
    }

    // Methods
    public long getId() {
        return id;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public ContractType getContractType() {
        return contractType;
    }

    public void setContractType(ContractType contractType) {
        this.contractType = contractType;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String toString() {
        return "Contract Type: " + getContractType() + ", Salary: " + getSalary();
    }


    // ENUM

    /**
     * Type of Contract
     */
    public enum ContractType {

        CONTRACT("Contract"),
        FREELANCE("Freelance");

        // Variables
        private final String CONTRACT_TYPE;

        // Constructor
        ContractType(String CONTRACT_TYPE) {
            this.CONTRACT_TYPE = CONTRACT_TYPE;
        }

        // Methods
        public String getCONTRACT_TYPE() {
            return CONTRACT_TYPE;
        }

        public String toString() {
            return getCONTRACT_TYPE();
        }
    }
}
