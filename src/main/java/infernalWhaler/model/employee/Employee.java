package infernalWhaler.model.employee;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Represents an Employee
 *
 * @see infernalWhaler.dataLayer.employeeDAO.EmployeeDAO
 * @see infernalWhaler.service.employeeServcie.EmployeeServcie
 * @see EmployeeWorkDetails
 * @see Contract
 */
@Entity
@Table(name = "Employee")
public class Employee {

    // Variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @Column(name = "First_Name", length = 40)
    private String firstName;
    @Column(name = "Last_name", length = 40)
    private String lastName;

    @Column(name = "Telephone", length = 10)
    private String telephone;

    @Column(name = "Birthday")
    private LocalDate birthday;

    @Column(name = "Address")
    private String address;
    @Column(name = "City")
    private String city;

    @OneToOne(cascade = {CascadeType.ALL,}, fetch = FetchType.EAGER)
    @JoinColumn(name = "Contract_ID")
    private Contract contract;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "EMP_WORK_ID")
    private EmployeeWorkDetails employeeWorkDetails;

    // Constructor
    public Employee() {
    }

    public Employee(String firstName, String lastName, String telephone, String address, String city, LocalDate birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
        this.address = address;
        this.city = city;
        this.birthday = birthday;
    }

    // Methods
    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Contract getContract() {
        return contract;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public void addContract(Contract contract) {
        this.contract = contract;
        contract.setEmployee(this);
    }

    public EmployeeWorkDetails getEmployeeWorkDetails() {
        return employeeWorkDetails;
    }

    public void setEmployeeWorkDetails(EmployeeWorkDetails employeeWorkDetails) {
        this.employeeWorkDetails = employeeWorkDetails;
    }


    public String toString() {
        return "Employee " + getId() + ": " + getLastName() + " " + getFirstName() +
                " [ Telephone: " + getTelephone() + ", Address: " + getAddress() + " " + getCity() + ", Birthday: " + getBirthday() + " ]" +
                " [ Contract: " + getContract() + " ]" +
                " [ Work details: " + getEmployeeWorkDetails() + " ]";
    }
}
